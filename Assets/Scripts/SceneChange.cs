﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneChange : MonoBehaviour
{
    public Scene scene;
    public int option;

    public Button button;
    void Start()
    {
        Button btn = button.GetComponent<Button>();
        switch (option)
        {
            case 1:
                button.onClick.AddListener(ChangeToScene);
                break;
            case 2:
                button.onClick.AddListener(ChangeToMenu);
                break;
        }
    }
    public void ChangeToScene()
    {
        SceneManager.LoadScene("Assets/Scenes/Pregunta.unity", LoadSceneMode.Single);
    }
    public void ChangeToMenu()
    {
        SceneManager.LoadScene("Assets/Scenes/Menu.unity", LoadSceneMode.Single);
    }
}