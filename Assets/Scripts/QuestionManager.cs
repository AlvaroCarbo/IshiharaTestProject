﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



[System.Serializable]
public class Question
{
    public Sprite ishiharaImages;
    public Dropdown.OptionData[] answers1;
    public int questionIndex;
    public string question;
    public string captionTextResposta;
}


public class QuestionManager : MonoBehaviour
{
    public Question[] question;
    public Text titol;
    public Image plateImage;
    public Text pregunta;
    public Dropdown resposta1;
    static int counter = 0;
    public Button nextButton;
    static int[] respostes = new int[38];
    // Start is called before the first frame update
    void Start()
    {
        

        if (SceneManager.GetActiveScene().name.Equals("Pregunta"))
        {
            setTitle();
            plateImage.sprite = question[counter].ishiharaImages;
            pregunta.text = question[counter].question;
            resposta1.captionText.text = question[counter].captionTextResposta;
            for (int i = 0; i < question[counter].answers1.Length; i++)
            {
                resposta1.options[i] = question[counter].answers1[i];
            }
            if (counter == question.Length - 1)
            {
                
                counter = 0;
                nextButton.onClick.AddListener(showResult);
            }
            else
            {
                nextButton.onClick.AddListener(ChangeQuestion);
            }
        }else
        {
            //pregunta.text = respostes[38].ToString();
            int totalNormal = 0;
            int totalRedGreenDefficiency = 0;
            int totalProtanopia = 0;
            int totalDeuteranopia = 0;
            int totalIncorrect = 0;
            for (int i = 0; i< respostes.Length; i++) 
            {
                if (i == 0 || i == 37)
                {
                    totalNormal++;
                }else if (i > 0 && i <= 20) 
                {
                    if (respostes[i] == 0)
                    {
                        totalNormal++;
                    }else if (respostes[i] == 1)
                    {
                        totalRedGreenDefficiency++;
                    } else
                    {
                        totalIncorrect++;
                    }
                }else if (i > 20 && i <= 26)
                {
                    if (respostes[i] == 0)
                    {
                        totalNormal++;
                    }
                    else if (respostes[i] == 1)
                    {
                        totalProtanopia++;
                    }
                    else
                    {
                        totalDeuteranopia++;
                    }
                }
                else if (i > 26 && i <= 36)
                {
                    if (respostes[i] == 0)
                    {
                        totalNormal++;
                    }
                    else if (respostes[i] == 1)
                    {
                        totalRedGreenDefficiency++;
                    }
                    else
                    {
                        totalIncorrect++;
                    }
                }
            }
            pregunta.text = "Correct answers: " + totalNormal.ToString() + "/" + respostes.Length.ToString()+ "\nRed-Green Defficiency answers: " + totalRedGreenDefficiency.ToString() +"/" + respostes.Length.ToString() + "\nProtanopia answers: " + totalProtanopia.ToString() + "/" + respostes.Length.ToString() + "\nDeuteranopia answers: " + totalDeuteranopia.ToString() + "/" + respostes.Length.ToString() + "\nIncorrect answers: " +totalIncorrect.ToString() + "/"+ respostes.Length.ToString();
        
    }
       


    }

    void setTitle()
    {
        titol.text = "Question " + question[counter].questionIndex + "/" + question.Length;
    }

    void showResult()
    {
        SceneManager.LoadScene("Resposta");
    }

    void ChangeQuestion()
    {
        respostes[counter] = resposta1.value;
        Debug.Log(resposta1.value);
        counter++;
        SceneManager.LoadScene("Pregunta");
    }

    // Update is called once per frame
    void Update()
    {
       
    }
}
